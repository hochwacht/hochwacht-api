module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  proxy: true,
  // url: env('PUBLIC_URL', 'https://apiv2.hochwacht.ch'),
  app: {
    keys: env.array('APP_KEYS'),
  },
  webhooks: {
    // Add this to not receive populated relations in webhooks
    populateRelations: true,
  },
});
