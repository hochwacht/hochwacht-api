import type { Schema, Struct } from '@strapi/strapi';

export interface ComponentFaq extends Struct.ComponentSchema {
  collectionName: 'components_component_faqs';
  info: {
    displayName: 'FAQ';
    icon: 'question';
  };
  attributes: {
    Questions: Schema.Attribute.Component<'entity.faq-question', true> &
      Schema.Attribute.Required;
  };
}

export interface EntityAddress extends Struct.ComponentSchema {
  collectionName: 'components_entity_addresses';
  info: {
    description: '';
    displayName: 'Address';
  };
  attributes: {
    City: Schema.Attribute.String & Schema.Attribute.Required;
    Country: Schema.Attribute.String;
    MapsEmbedUrl: Schema.Attribute.String;
    Number: Schema.Attribute.String;
    Street: Schema.Attribute.String & Schema.Attribute.Required;
    Zip: Schema.Attribute.String & Schema.Attribute.Required;
  };
}

export interface EntityContact extends Struct.ComponentSchema {
  collectionName: 'components_person_contacts';
  info: {
    description: '';
    displayName: 'Contact';
    icon: 'walk';
  };
  attributes: {
    Email: Schema.Attribute.Email;
    Name: Schema.Attribute.String;
    PhoneNumber: Schema.Attribute.String;
  };
}

export interface EntityDocument extends Struct.ComponentSchema {
  collectionName: 'components_entity_documents';
  info: {
    displayName: 'Document';
    icon: 'filePdf';
  };
  attributes: {
    Document: Schema.Attribute.Media<'images' | 'files' | 'videos' | 'audios'> &
      Schema.Attribute.Required;
    Title: Schema.Attribute.String & Schema.Attribute.Required;
  };
}

export interface EntityFaqQuestion extends Struct.ComponentSchema {
  collectionName: 'components_entity_faq_questions';
  info: {
    description: '';
    displayName: 'FaqQuestion';
    icon: 'question';
  };
  attributes: {
    Answer: Schema.Attribute.Blocks & Schema.Attribute.Required;
    Question: Schema.Attribute.String & Schema.Attribute.Required;
  };
}

export interface LayoutMarkdownSection extends Struct.ComponentSchema {
  collectionName: 'components_layout_markdown_sections';
  info: {
    description: '';
    displayName: 'Markdown Section';
    icon: 'apps';
  };
  attributes: {
    content: Schema.Attribute.RichText & Schema.Attribute.Required;
    title: Schema.Attribute.String & Schema.Attribute.Required;
  };
}

export interface LayoutSection extends Struct.ComponentSchema {
  collectionName: 'components_layout_sections';
  info: {
    description: '';
    displayName: 'Block Section';
    icon: 'apps';
  };
  attributes: {
    content: Schema.Attribute.Blocks & Schema.Attribute.Required;
    title: Schema.Attribute.String & Schema.Attribute.Required;
  };
}

export interface ResourceCenterContactSection extends Struct.ComponentSchema {
  collectionName: 'components_resource_center_contact_sections';
  info: {
    description: '';
    displayName: 'ContactSection';
    icon: 'envelop';
  };
  attributes: {
    Column: Schema.Attribute.Boolean &
      Schema.Attribute.Required &
      Schema.Attribute.DefaultTo<false>;
    Contacts: Schema.Attribute.Component<'entity.contact', true>;
  };
}

export interface ResourceCenterDocumentSection extends Struct.ComponentSchema {
  collectionName: 'components_resource_center_document_sections';
  info: {
    description: '';
    displayName: 'DocumentSection';
    icon: 'filePdf';
  };
  attributes: {
    DisplayMode: Schema.Attribute.Enumeration<['card', 'button', 'list']> &
      Schema.Attribute.Required &
      Schema.Attribute.DefaultTo<'list'>;
    Documents: Schema.Attribute.Component<'entity.document', true> &
      Schema.Attribute.Required;
  };
}

export interface ResourceCenterLeaderSection extends Struct.ComponentSchema {
  collectionName: 'components_resource_center_leader_sections';
  info: {
    description: '';
    displayName: 'LeaderSection';
    icon: 'user';
  };
  attributes: {
    Column: Schema.Attribute.Boolean &
      Schema.Attribute.Required &
      Schema.Attribute.DefaultTo<true>;
    leaders: Schema.Attribute.Relation<'oneToMany', 'api::leader.leader'>;
  };
}

export interface UtilStaticComponent extends Struct.ComponentSchema {
  collectionName: 'components_util_static_components';
  info: {
    displayName: 'StaticComponent';
    icon: 'layer';
  };
  attributes: {
    ComponentName: Schema.Attribute.Enumeration<['faq']> &
      Schema.Attribute.Required;
  };
}

declare module '@strapi/strapi' {
  export module Public {
    export interface ComponentSchemas {
      'component.faq': ComponentFaq;
      'entity.address': EntityAddress;
      'entity.contact': EntityContact;
      'entity.document': EntityDocument;
      'entity.faq-question': EntityFaqQuestion;
      'layout.markdown-section': LayoutMarkdownSection;
      'layout.section': LayoutSection;
      'resource-center.contact-section': ResourceCenterContactSection;
      'resource-center.document-section': ResourceCenterDocumentSection;
      'resource-center.leader-section': ResourceCenterLeaderSection;
      'util.static-component': UtilStaticComponent;
    }
  }
}
