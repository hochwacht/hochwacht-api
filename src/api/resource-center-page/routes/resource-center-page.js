'use strict';

/**
 * resource-center-page router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resource-center-page.resource-center-page');
