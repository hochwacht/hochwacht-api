'use strict';

/**
 * resource-center-page service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resource-center-page.resource-center-page');
