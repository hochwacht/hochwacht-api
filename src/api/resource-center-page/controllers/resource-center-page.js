'use strict';

/**
 * resource-center-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resource-center-page.resource-center-page');
