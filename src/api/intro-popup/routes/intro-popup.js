'use strict';

/**
 * intro-popup router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::intro-popup.intro-popup');
