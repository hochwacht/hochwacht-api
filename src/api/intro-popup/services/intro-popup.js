'use strict';

/**
 * intro-popup service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::intro-popup.intro-popup');
