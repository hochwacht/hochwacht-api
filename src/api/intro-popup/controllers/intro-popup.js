'use strict';

/**
 *  intro-popup controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::intro-popup.intro-popup');
