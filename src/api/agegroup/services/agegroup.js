'use strict';

/**
 * agegroup service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::agegroup.agegroup');
