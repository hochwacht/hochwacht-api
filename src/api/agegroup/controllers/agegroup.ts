'use strict';
const moment = require('moment')
const ical = require('ical-generator')



/**
 *  agegroup controller
 */

import { factories } from "@strapi/strapi";

export default factories.createCoreController('api::agegroup.agegroup');
