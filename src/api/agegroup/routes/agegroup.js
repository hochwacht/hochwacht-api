'use strict';

/**
 * agegroup router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::agegroup.agegroup');
