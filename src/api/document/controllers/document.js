'use strict';

/**
 *  document controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::document.document', ({ strapi }) => ({
    async find(ctx) {
        // the big boy version of getting stuff from the API:
        //https://docs.strapi.io/developer-docs/latest/developer-resources/database-apis-reference/query-engine-api.html
        return strapi.db.query('api::document.document').findMany({
            where: {
                publishedAt: { $ne: null },
            },
            populate: {
                file: true,
                thumbnail: true,
            },
        });
    },

    async findOne(ctx) {
        const { id } = ctx.params;
        const { query } = ctx;

        const entity = await strapi.service('api::document.document').findOne(id, query);
        return await this.sanitizeOutput(entity, ctx);
    }
}));